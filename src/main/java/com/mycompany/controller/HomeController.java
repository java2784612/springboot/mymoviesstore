package com.mycompany.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/form")
    public String redirectionForm() {
        return "form";
    }

    @GetMapping("/test")
    public String redirectionTest() {
        return "test";
    }

}
