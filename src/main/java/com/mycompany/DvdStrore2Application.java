package com.mycompany;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DvdStrore2Application {

    public static void main(String[] args) {

        SpringApplication.run(DvdStrore2Application.class, args);
    }

}
